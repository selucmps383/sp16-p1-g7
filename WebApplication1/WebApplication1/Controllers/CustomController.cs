﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
    public class CustomController : Controller
    {

        private TableContext db = new TableContext();
        
        // GET: Custom
        public ActionResult People()
        {
            return View(db.Users.ToList());
        }
        public ActionResult Product()
        {


            return View(db.Products.ToList());
        }

}
}