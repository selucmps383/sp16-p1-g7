﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace WebApplication1.Models
{
    public class User
    {
        [Key]
        
        public int UserId { get; set; }
        [Required(ErrorMessage= "User Name: Required.")]
        public string Username { get; set; }
        [Required(ErrorMessage = "First Name: Required.")]
        public string FirstName { get; set; }
        [Required(ErrorMessage = "Last Name: Required")]
        public string LastName { get; set; }
        [Required(ErrorMessage = "Password: Required.")]
        [DataType(DataType.Password)]
        public string Password { get; set; }
        [Required(ErrorMessage = "Password Confirmation: Required.")]
        [Compare ("Password",ErrorMessage = "Password Confirmation: Does not Match.")]
        [DataType(DataType.Password)]
        public string PasswordCheck { get; set; }
        
        public bool Admin { get; set; }

       


    
}
} 