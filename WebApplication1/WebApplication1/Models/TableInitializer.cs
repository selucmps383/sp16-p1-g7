﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace WebApplication1.Models
{
    public class TableInitializer  : DropCreateDatabaseIfModelChanges<TableContext>
    {

        protected override void Seed(TableContext context)
        {

            var Products = new List<Product>
            {
                new Product { ProductName= "Muffins" ,AmountofProduct= 9001 },
                new Product { ProductName= "Juice" ,AmountofProduct= 2055  },
                new Product { ProductName= "NotJunk", AmountofProduct= 12 },
             };
            
            foreach (var temp in Products)
            {
                context.Products.Add(temp);
            }

            context.SaveChanges();

            var Users = new List<User>
            {
               
                new User { Username="berryD",FirstName= "Berry",LastName= "Dedic",Password="Easteregg",Admin=false},
                new User { Username="JSwammy",FirstName= "John",LastName= "Doe",Password="selu2014",Admin=true},
                new User { Username="PManning",FirstName= "Peyton",LastName= "Manning",Password="TouchDown",Admin=false},
                new User { Username="EPreZ",FirstName= "Elvis",LastName= "Presley",Password="Memphis",Admin=false}
             };
            
            foreach (var temp in Users)
            {
                context.Users.Add(temp);
            }
            context.SaveChanges();


        }




    }
}